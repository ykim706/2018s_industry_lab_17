package ictgradschool.industry.testingandrefactoring.ex02;

import ictgradschool.Keyboard;

public class ShapeAreaCalculator {

    public void start() {
        System.out.println("Welcome to Shape Area Calculator!");
        System.out.println("");
        System.out.print("Enter the width of the rectangle: ");
        String width = Keyboard.readInput();
        System.out.print("Enter the length of the rectangle: ");
        String length = Keyboard.readInput();
        System.out.println("");
        System.out.print("The radius of the circle is : ");
        String radius = Keyboard.readInput();
        Double convertedWidth = convertToDouble(width);
        Double convertedLength = convertToDouble(length);
        Double convertedRadius = convertToDouble(radius);
        Double areaRect = areaRectangle(convertedWidth, convertedLength);
        Double areaCir = areaCircle(convertedRadius);
        int roundedRect = roundingArea(areaRect);
        int roundedCir = roundingArea(areaCir);
        int smallerArea = smallerArea(roundedCir, roundedRect);
        System.out.print("The smaller area is: " + smallerArea);

    }


    public double convertToDouble(String s) {
        Double converted;
        converted = Double.parseDouble(s);
        return converted;
    }

    public double areaRectangle(double width, double length) {
        double areaRectangle = width * length;
        return areaRectangle;
    }

    public double areaCircle(double radius) {
        double areaCircle = Math.PI * radius * radius;
        return areaCircle;
    }

    public int roundingArea(double area) {
        int roundedArea = (int) Math.round(area);
        return roundedArea;
    }

    public int smallerArea(int areaCircle, int areaRectangle) {
        int smallerArea = Math.min(areaCircle, areaRectangle);
        return smallerArea;
    }

    public static void main(String[] args) {
        ShapeAreaCalculator shapeAreaCalculator = new ShapeAreaCalculator();
        shapeAreaCalculator.start();
    }

}

package ictgradschool.industry.testingandrefactoring.ex01;

import ictgradschool.industry.testingandrefactoring.ex02.ShapeAreaCalculator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ShapeAreaCalculatorTest {
    private ShapeAreaCalculator shapeAreaCalculator;


    @Before
    public void initialize() {
        shapeAreaCalculator = new ShapeAreaCalculator();
    }

    @Test
    public void convertToDoubleTest() {
        assertEquals(20.5, shapeAreaCalculator.convertToDouble("20.5"), 0.01);
    }

    @Test
    public void areaRectangleTest() {
        assertEquals(150, shapeAreaCalculator.areaRectangle(15, 10), 0.01);
    }

    @Test
    public void areaCircleTest() {
        assertEquals(1256.63, shapeAreaCalculator.areaCircle(20), 0.01);
    }


    @Test
    public void roundingAreaTest() {
        assertEquals(5, shapeAreaCalculator.roundingArea(5.3), 0.01);
    }

    @Test
    public void smallerAreaTest() {
        assertEquals(359, shapeAreaCalculator.smallerArea(359, 379), 0.01);
    }


}

package ictgradschool.industry.testingandrefactoring.ex01;

import ictgradschool.industry.testingandrefactoring.ex03.ExcelNew;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ExcelNewTest {
    private ExcelNew excelNew;

    @Before
    public void setUp() {
        excelNew = new ExcelNew();
    }

    @Test
    public void testGetString(){
        assertEquals("student91",excelNew.getString("student", 1, 5, 15, 25, 65));

    }


}
